angular.module('app.controllers', [])
  
.controller('bILLPAYCtrl', function($scope) {

})
   
.controller('recentPaymentsCtrl', function($scope) {

})
   
.controller('scheduledPaymentsCtrl', function($scope) {

})
         
.controller('sAPCtrl', function($scope, $ionicPopup, $timeout,$http, $ionicPlatform) {
console.log('inside sap conr=troller....');
$scope.submit = function() {
var payeeName;
var pmtAmt;
var faName;
$scope.servRes='';
 //console.log('inside submit function '+$scope.speechText);
 var confirmPopup = $ionicPopup.confirm({
     title: 'Payment confirmation',
     template: 'Are you sure you want to pay now?'
   });

   confirmPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
	   if ($scope.speechText) {
          console.log('submtted text is =='+$scope.speechText);
		  //call to speech text validation method here :A standard payment instruction must be like 
		  // "Pay payee_nickname bucks payment_amount from funding account funding_account_name"
		  //
			var strLength;
			var str = $scope.speechText;	
			str=str.trim();
			strLength = str.length;
			
			var indPay= str.indexOf("pay");
			var indBucks= str.indexOf("bucks");
			var indFrom= str.indexOf("from");
			var indAcct = str.indexOf("account");
			if(str.indexOf("Pay")!=-1 && str.indexOf("bucks")!=-1 && str.indexOf("from")!=-1 && str.indexOf("account")!= -1){
				payeeName= str.substring(indPay+4,indBucks);
				pmtAmt= str.substring(indBucks+5,indFrom);
				faName= str.substring(indAcct+7,strLength);
				 console.log('You are sure'+payeeName+'--'+pmtAmt+'--'+faName);
				 if(payeeName && pmtAmt && faName){
					//if payeeName && pmtAmt && faName all are valid then only we'll go for payment and hit service otherwise show user error msg.
					   $http.get("http://10.74.224.192:8080/SpringRestService/makePayment/3/300/2345").then(function (response) {
						$scope.returnedResponse = response.data;
						console.log('returnedResponse===='+$scope.returnedResponse);
						if($scope.returnedResponse){
							$scope.servRes='Congratulation, Your payment is successful. Please note your payment id '+$scope.returnedResponse.pmtId+ ' for future reference.';
							//console.log('inside serv response if');
						    $scope.speechText = '';
						}else{
						   $scope.servRes='Some error has occured,Please try again.'
						}
						  
					 });   
				  }
			 
        }else{
			   var alertPopup = $ionicPopup.alert({
				 title: 'Invalid Inputs.',
				 template: 'Please enter valid PayeeName, Payment Amount and Funding Account.'
			   });

			   alertPopup.then(function(res) {
				 console.log('Please enter valid PayeeName, Payment Amount and Funding Account.Logs');
				 $scope.speechText = '';
			   });
 		}
       
  }else{
   var alertPopup1 = $ionicPopup.alert({
				 title: 'Invalid Inputs.',
				 template: 'Please enter a valid statement.'
			   });

			   alertPopup1.then(function(res) {
				 console.log('Please enter a valid statement. alert');
				 $scope.speechText = '';
			   });
  }

     } else {
       console.log('You are not sure');
     }
   });
   };
    
	
	   
	  /*
// A confirm dialog
 $scope.showConfirm = function() {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Payment confirmation',
     template: 'Are you sure you want to pay now?'
   });

   confirmPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
	   if ($scope.speechText) {
          console.log('submtted text is =='+this.speechText);
          $scope.speechText = '';
        }
        $http.get("http://www.w3schools.com/angular/customers.php").then(function (response) {
        $scope.returnResponse = response.data.records;
  });

     } else {
       console.log('You are not sure');
     }
   });
   };
   */
   
   
/*var recognition = new webkitSpeechRecognition();
recognition.lang = "en-IN";
recognition.continuous = true;
recognition.interimResults = true;
console.log('above start1');
recognition.start();
console.log('after start1');
recognition.onresult = function(event) { 
  console.log("result is--"+event) ;
}
console.log('after start2');
*/

/*
var recognition;

$ionicPlatform.ready(function() {
  console.log('inside function ready');
    recognition = new SpeechRecognition();
    recognition.onresult = function(event) {
        if (event.results.length > 0) {
		  console.log('inside if');
            q.value = event.results[0][0].transcript;
			  console.log(q.value);
            q.form.submit();
        }
    }
});
*/


  
/*
var langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];
for (var i = 0; i < langs.length; i++) {
  select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 6;
updateCountry();
select_dialect.selectedIndex = 6;
showInfo('info_start');

function updateCountry() {
  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}
var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'mic-animate.gif';
  };
  
  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = '/img/mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = '/img/mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };
  
  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = '/img/mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
    if (create_email) {
      create_email = false;
      createEmail();
    }
  };
  
  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
    if (final_transcript || interim_transcript) {
      showButtons('inline-block');
    }
  };
}
function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}
var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}
function createEmail() {
  var n = final_transcript.indexOf('\n');
  if (n < 0 || n >= 80) {
    n = 40 + final_transcript.substring(40).indexOf(' ');
  }
  var subject = encodeURI(final_transcript.substring(0, n));
  var body = encodeURI(final_transcript.substring(n + 1));
  window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
}
$scope.copyButton = function() {
  if (recognizing) {
    recognizing = false;
    recognition.stop();
  }
  copy_button.style.display = 'none';
  copy_info.style.display = 'inline-block';
  showInfo('');
};
$scope.emailButton = function() {
  if (recognizing) {
    create_email = true;
    recognizing = false;
    recognition.stop();
  } else {
    createEmail();
  }
  email_button.style.display = 'none';
  email_info.style.display = 'inline-block';
  showInfo('');
};

$scope.startButton = function(event) {
console.log('start button method invoked');
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.lang = select_dialect.value;
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = '/img/mic.gif';
  showInfo('info_allow');
  showButtons('none');
  //start_timestamp = event.timeStamp;
  console.log('start button method end');
};
function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}
var current_style;
function showButtons(style) {
  if (style == current_style) {
    return;
  }
  current_style = style;
  copy_button.style.display = style;
  email_button.style.display = style;
  copy_info.style.display = 'none';
  email_info.style.display = 'none';
}
*/
 
})
 